<?php
/**
 * 主数据库连接参数，未配置的参数使用框架惯性配置
 * 如果修改为mysql数据库，请同时修改type和dbname两个参数
 */
return array(

    'database' => array(
        'type' => 'mysqli', // Database connection driver type：mysqli,sqlite,pdo_mysql,pdo_sqlite
        'host' => '127.0.0.1', // database server
        'user' => 'root', // Database connection user name
        'passwd' => 'Tr4iningB!', // Database connection password
        'port' => '3306', // Database port
        'dbname' => 'pbootcms' // Database name
    )

);